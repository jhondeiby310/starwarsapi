var req = new XMLHttpRequest();
req.open('GET', 'https://swapi.co/api/films/1/?format=json', false);
req.send(null);
if (req.status == 200){
let data = JSON.parse(req.responseText);
let button = document.querySelector('#button');
let title = document.querySelector('#title');
let opening_crawl = document.querySelector('#opening_crawl');
let director = document.querySelector('#director');
let release_date = document.querySelector('#release_date');

function getInfo() {
    updateInfoWithLoading()
    let number = parseInt(prompt("Enter the movie number"))
    let apiUrl = 'https://swapi.co/api/films/' + number;

    axios.get(apiUrl).then(response =>{
        updateInfo(response.data);
    }).catch(e => {
        updateInfoWithError();
    })
}

function updateInfo(data) {
    title.innerText = data.title;
    opening_crawl.innerHTML = data.opening_crawl;
    director.innerText = `Director: ${data.director}`;
    release_date.innerText = `Realse Date: ${data.release_date}`;
}

function updateInfoWithError(data) {
    title.innerText = 'We are sorry! This movie is not available';
    opening_crawl.innerText = '';
    director.innerText = '';
    release_date.innerText = '';
}

function updateInfoWithLoading(data) {
    title.innerHTML = 'LOADING...';
    opening_crawl.innerText = '';
    director.innerText = '';
    release_date.innerText = '';
}

button.addEventListener('click', getInfo)
}